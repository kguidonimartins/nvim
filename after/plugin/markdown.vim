" https://github.com/kguidonimartins/indentLine need conceallevel=2
" to show the indented lines, but the command below do not work.
" check: verbose set conceallevel
autocmd BufNewFile,BufRead *.md setlocal conceallevel=0
let g:markdown_fenced_languages = ['html', 'python', 'r']
