" autocmd BufNewFile,BufRead *.Rmd syntax match rmdYamlBlockDelim /\%^---\_.\{-}---$/

" Fold expressions {{{1
function! StackedRmdFolds()
  let thisline = getline(v:lnum)
  let prevline = getline(v:lnum - 1)
  let nextline = getline(v:lnum + 1)
  if thisline =~ '^```{.*$' && prevline =~ '^\s*$'  " start of a fenced block
    return ">2"
  elseif thisline =~ '^```$' && nextline =~ '^\s*$'  " end of a fenced block
    return "<2"
  endif

  if HeadingDepth(v:lnum) > 0
    return ">1"
  else
    return "="
  endif
endfunction

function! NestedRmdFolds()
  let thisline = getline(v:lnum)
  let prevline = getline(v:lnum - 1)
  let nextline = getline(v:lnum + 1)
  if thisline =~ '^```.*$' && prevline =~ '^\s*$'  " start of a fenced block
    return "a1"
  elseif thisline =~ '^```$' && nextline =~ '^\s*$'  " end of a fenced block
    return "s1"
  endif

  let depth = HeadingDepth(v:lnum)
  if depth > 0
    return ">".depth
  else
    return "="
  endif
endfunction

" Helpers {{{1
function! s:SID()
  return matchstr(expand('<sfile>'), '<SNR>\d\+_')
endfunction

function! HeadingDepth(lnum)
  let level=0
  let thisline = getline(a:lnum)
  if thisline =~ '^#\+\s\+'
    let hashCount = len(matchstr(thisline, '^#\{1,6}'))
    if hashCount > 0
      let level = hashCount
    endif
  " else
    " if thisline != ''
    "   let nextline = getline(a:lnum + 1)
    "   if nextline =~ '^=\+\s*$'
    "     let level = 1
    "   " elseif nextline =~ '^-\+\s*$'
    "   "   let level = 2
    "   endif
    " endif
  endif
  if level > 0 && LineIsFenced(a:lnum)
    " Ignore # or === if they appear within fenced code blocks
    let level = 0
  endif
  return level
endfunction

function! LineIsFenced(lnum)
  if exists("b:current_syntax") && b:current_syntax ==# 'rmd'
    " It's cheap to check if the current line has 'rmdCode' syntax group
    return HasSyntaxGroup(a:lnum, '\vrComment|htmlCommentPart|pandocYAMLHeader')
  else
    " Using searchpairpos() is expensive, so only do it if syntax highlighting
    " is not enabled
    return s:HasSurroundingFencemarks(a:lnum)
  endif
endfunction

function! HasSyntaxGroup(lnum, targetGroup)
  let syntaxGroup = map(synstack(a:lnum, 1), 'synIDattr(v:val, "name")')
  for value in syntaxGroup
    if value =~ a:targetGroup
        return 1
    endif
  endfor
endfunction

function! s:HasSurroundingFencemarks(lnum)
  let cursorPosition = [line("."), col(".")]
  call cursor(a:lnum, 1)
  let startFence = '\%^```{\|^\n\zs```{'
  let endFence = '```\n^$'
  let fenceEndPosition = searchpairpos(startFence,'',endFence,'W')
  call cursor(cursorPosition)
  return fenceEndPosition != [0,0]
endfunction

function! s:FoldText()
  let level = HeadingDepth(v:foldstart)
  let indent = repeat('#', level)
  let title = substitute(getline(v:foldstart), '^#\+\s\+', '', '')
  let foldsize = (v:foldend - v:foldstart)
  let linecount = '['.foldsize.' line'.(foldsize>1?'s':'').']'

  if level < 6
    let spaces_1 = repeat(' ', 6 - level)
  else
    let spaces_1 = ' '
  endif

  if exists('*strdisplaywidth')
      let title_width = strdisplaywidth(title)
  else
      let title_width = len(title)
  endif

  if title_width < 40
    let spaces_2 = repeat(' ', 40 - title_width)
  else
    let spaces_2 = ' '
  endif

  return indent.spaces_1.title.spaces_2.linecount
endfunction

" API {{{1
function! ToggleRmdFoldexpr()
  if &l:foldexpr ==# 'StackedRmdFolds()'
    setlocal foldexpr=NestedRmdFolds()
  else
    setlocal foldexpr=StackedRmdFolds()
  endif
endfunction
command! -buffer FoldRmdToggle call ToggleRmdFoldexpr()

" Setup {{{1
if !exists('g:rmd_fold_style')
  let g:rmd_fold_style = 'stacked'
endif

if !exists('g:rmd_fold_override_foldtext')
  let g:rmd_fold_override_foldtext = 1
endif

setlocal foldmethod=expr

if g:rmd_fold_override_foldtext
  let &l:foldtext = s:SID() . 'FoldText()'
endif

let &l:foldexpr =
  \ g:rmd_fold_style ==# 'nested'
  \ ? 'NestedRmdFolds()'
  \ : 'StackedRmdFolds()'

" Teardown {{{1
if !exists("b:undo_ftplugin") | let b:undo_ftplugin = '' | endif
let b:undo_ftplugin .= '
  \ | setlocal foldmethod< foldtext< foldexpr<
  \ | delcommand FoldRmdToggle
  \ '

function! RmdFoldText()
    let offset = 8
    let line = getline(v:foldstart)
    let line2 = getline(v:foldstart + 1)
    let sub = substitute(line . line2, '```{\|}\|#\|/\*\|\*/\|{{{\d\=', '', 'g')
    let hashcount = count(getline(v:foldstart), "#")
    let rmdchunk = count(getline(v:foldstart), "```{")
    if rmdchunk == 1
           let rmdchunk = 4
    endif
    let ind = indent(v:foldstart)
    let lines = v:foldend-v:foldstart + 1
    let i = 0
    let spaces = ''
    while i < (ind - ind/4)
        let spaces .= ' '
        let i = i+1
    endwhile
    " return spaces . repeat(' ', hashcount) . repeat(' ', rmdchunk) . sub . repeat('-', winwidth(0) - strlen(spaces . sub) - offset - hashcount - rmdchunk) . '('. lines .')'
    return spaces . repeat(' ', hashcount) . repeat(' ', rmdchunk) . sub . ' ▾' . repeat(' ', winwidth(0) - strlen(spaces . sub))
endfunction


" vim:set fdm=marker:
