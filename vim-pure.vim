syntax on
set hidden
set mouse=a
set clipboard=unnamed,unnamedplus

" hi! StatusLine cterm=bold ctermbg=238 gui=bold guibg=#424450
" hi String      ctermfg=228 guifg=#F1FA8C

hi Search    cterm=bold ctermfg=11 ctermbg=9 guifg=yellow guibg=red gui=bold
hi IncSearch cterm=bold ctermfg=11 ctermbg=9 guifg=yellow guibg=red gui=bold

nnoremap <silent><esc> :nohlsearch<CR>

nnoremap n nzz
nnoremap N Nzz
nnoremap * *Nzz

nnoremap <F2>  :normal! g`"<CR>
nnoremap <F3> <Esc>:syntax sync fromstart<CR>

nnoremap <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
            \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
            \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<cr>

autocmd BufRead,BufNewFile * syntax sync fromstart

source ~/.config/nvim/plugin/tmux.vim
