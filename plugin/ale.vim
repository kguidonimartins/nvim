"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'dense-analysis/ale'                           " great linter for several languages
"------------------------------------------------------------------------------------------------------------------------------------
noremap <leader>dg :GitGutterToggle \| ALEToggle <CR>
noremap <leader>ta :ALEToggle <CR>
nmap <F9> <Plug>(ale_fix)
let g:ale_fix_on_save = 0
let g:ale_open_list = 0
let g:ale_keep_list_window_open = 1
let g:ale_change_sign_column_color = 1
let g:ale_list_window_size = 3
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%: %severity%] %s'
let g:ale_lint_on_text_changed = 'never'
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 1
let g:ale_fixers = {
\   '*': [
\       'remove_trailing_lines',
\       'trim_whitespace'
\   ],
\   'python': [
\       'isort',
\       'yapf'
\   ],
\   'r': [
\       'styler'
\   ],
\   'rmd': [
\       'styler'
\   ]
\}
" let g:ale_set_highlights = 0
let g:ale_r_lintr_options = "with_defaults(line_length_linter(120), object_name_linter = NULL)"
let b:ale_exclude_highlights = [
            \ 'Lines should not be more than 80 characters.',
            \ 'Trailing whitespace is superfluous.',
            \ 'Commented code should be removed.'
            \ ]
highlight clear ALEErrorSign
highlight clear ALEWarningSign
let g:ale_sign_highlight_linenrs = 0
let g:ale_virtualtext_cursor = 0
let g:ale_set_highlights = 0
let g:ale_sign_error = 'x'
let g:ale_sign_warning = '.'
let g:ale_sign_info = '.'
let g:ale_sign_style_error = 'x'
let g:ale_sign_style_warning = '.'

" highlight colors were changed in the source code of dracula colorscheme
" see: ~/.local/lib/nvim/plug/dracula/after/plugin/dracula.vim
" ```
" if exists('g:ale_enabled')
"   hi! link ALEError              DraculaErrorLine
"   hi! link ALEWarning            DraculaWarnLine
"   hi! link ALEInfo               DraculaInfoLine

"   hi ALEErrorSign   gui=bold guibg=NONE guifg=red
"   hi ALEWarningSign gui=bold guibg=NONE guifg=yellow
"   hi ALEInfoSign    gui=bold guibg=NONE guifg=yellow

"   hi! link ALEVirtualTextError   Comment
"   hi! link ALEVirtualTextWarning Comment
" endif
" ```
