" ---------------------------------------------------------------------------
"  Colorscheme
" ---------------------------------------------------------------------------

"" https://gist.github.com/romainl/379904f91fa40533175dfaec4c833f2f
"function! FunkHighlights() abort
"        source ~/.config/nvim/colors/custom_highlights.vim
"endfunction
"
"augroup FunkColors
"    autocmd!
"    autocmd ColorScheme * call FunkHighlights()
"augroup END
"
"function! ChangeStatus() abort
"    if &modified
"        hi! link User2 FileChanged
"    else
"        hi! link User2 FileUnchanged
"    endif
"endfunction
"
"augroup ChangesStatusLineColorsBasedOnFileStatus
"    au InsertEnter              * hi! link User2 InsertUnchanged
"    au CursorMovedI,CursorHoldI * if !&modified | hi! link User2 InsertUnchanged | else | call ChangeStatus() | endif
"    au InsertChange,InsertLeave * call ChangeStatus()
"    au BufWritePre,BufWritePost * call ChangeStatus()
"    au BufReadPre,BufReadPost   * call ChangeStatus()
"    au CursorMoved,CursorHold   * call ChangeStatus()
"augroup END

" Funk prefered colorschemes
" colorscheme space_vim_theme
" colorscheme dracula

augroup TodoHighlight
    au!
    " case insensitive
    au Syntax * syn match TODO /\v\c<(beg|end|idea|ideia|review|todo|fix|fixme|change|tweak|note|optimize|problem|issue|warning|bug|source|task|update|include|test|testing|wip|see|check)('|s|es|ed|d)*:/
                \ containedin=.*Comment,vimCommentTitle,htmlCommentPart
    " case sensitive
    au Syntax * syn match TODO /\v<(FIXME|NOTE|TODO|OPTIMIZE|XXX|WIP):/
                \ containedin=.*Comment,vimCommentTitle,htmlCommentPart
    au Syntax * syn match Delimiter /\\\<pagebreak\>/
augroup END

nnoremap <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
            \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
            \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<cr>

au InsertEnter * set cursorline
au InsertLeave * set nocursorline

autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

augroup highlight_yank
    autocmd!
    au TextYankPost * silent! lua vim.highlight.on_yank{higroup="CursorLine"}
augroup END

" echo synIDattr(synID(line("."), col("."), 1), "name")

lua << EOF
require'colorizer'.setup()
EOF
