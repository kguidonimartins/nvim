"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'airblade/vim-gitgutter'                       " check git changes in gutter
"------------------------------------------------------------------------------------------------------------------------------------
" let g:gitgutter_sign_column_always = 1
" let g:gitgutter_map_keys = 0
" nnoremap <leader>tg :GitGutterToggle<CR>
" nnoremap ]gn :GitGutterNextHunk<CR>
" nnoremap [gp :GitGutterPrevHunk<CR>
" nnoremap <leader>hs :GitGutterStageHunk
" nnoremap <leader>hu :GitGutterUndoHunk
" command! Gqf GitGutterQuickFix | copen
