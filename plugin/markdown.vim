""------------------------------------------------------------------------------------------------------------------------------------
"" Plug 'tpope/vim-markdown'                           " more markdown
""------------------------------------------------------------------------------------------------------------------------------------
"autocmd FileType markdown setlocal commentstring=<!--\ %s\ -->
"let g:markdown_fenced_languages = ['html', 'python', 'bash=sh', 'r', 'rmd']

"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'plasticboy/vim-markdown'                      " markdown syntax
"------------------------------------------------------------------------------------------------------------------------------------
let g:vim_markdown_conceal=0 " plasticboy/vim-markdown dont hide my shit
let g:vim_markdown_conceal_code_blocks = 0 " plasticboy/vim-markdown dont hide my shit

"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'JamshedVesuna/vim-markdown-preview'           " preview
"------------------------------------------------------------------------------------------------------------------------------------
let vim_markdown_preview_hotkey='<C-m>'
let vim_markdown_preview_use_xdg_open=1
let vim_markdown_preview_github=1

"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'vim-pandoc/vim-pandoc-syntax'                 " more markdown
"------------------------------------------------------------------------------------------------------------------------------------
let g:pandoc#syntax#conceal#use=0

"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug', 'rmd'] }
"------------------------------------------------------------------------------------------------------------------------------------
let g:mkdp_filetypes = ['markdown', 'rmd']
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 0,
    \ 'sequence_diagrams': {},
    \ 'flowchart_diagrams': {},
    \ 'content_editable': v:true,
    \ 'disable_filename': 0
    \ }
