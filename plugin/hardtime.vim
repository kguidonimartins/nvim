let g:hardtime_default_on = 1
let g:hardtime_ignore_buffer_patterns = [ "CustomPatt[ae]rn", "NERD.*", "[coc-explorer*"]
let g:hardtime_ignore_quickfix = 1
let g:hardtime_maxcount = 6
