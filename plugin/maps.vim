nnoremap c "_c

nnoremap <Leader>vs :source $MYVIMRC<CR>
nnoremap <Leader>ve :e $MYVIMRC<CR>

nnoremap <tab>   za
" nnoremap <S-tab> <c-w>W

" Compile document, be it groff/LaTeX/markdown/etc.
map <F5> :w! \| !compiler <c-r>%<CR>

" Save file as sudo on files that require root permission
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" save and chmod file
cnoremap wch execute 'write !chmod +x %'<CR>
" cnoremap l execute 'Buffers'<CR>

" toggle cursorline
map <leader><F3> :set cursorline!<CR>

inoremap jj <Esc>

if has('nvim-0.5')
lua << EOF
        -- Add move line shortcuts: up and down
        vim.api.nvim_set_keymap('n', '<C-j>', ':m .+1<CR>==', { noremap = true})
        vim.api.nvim_set_keymap('n', '<C-k>', ':m .-2<CR>==', { noremap = true})
        vim.api.nvim_set_keymap('i', '<C-j>', '<Esc>:m .+1<CR>==gi', { noremap = true})
        vim.api.nvim_set_keymap('i', '<C-k>', '<Esc>:m .-2<CR>==gi', { noremap = true})
        vim.api.nvim_set_keymap('v', '<C-j>', ':m \'>+1<CR>gv=gv', { noremap = true})
        vim.api.nvim_set_keymap('v', '<C-k>', ':m \'<-2<CR>gv=gv', { noremap = true})
        -- Add move line shortcuts: left and right
        vim.api.nvim_set_keymap('n', '<C-h>', '<<', { noremap = true})
        vim.api.nvim_set_keymap('n', '<C-l>', '>>', { noremap = true})
        vim.api.nvim_set_keymap('v', '<C-h>', '<<', { noremap = true})
        vim.api.nvim_set_keymap('v', '<C-l>', '>>', { noremap = true})
EOF
else
        " Moving lines
        nnoremap <silent> <C-S-h> <<
        nnoremap <silent> <C-S-j> :move+<cr>
        nnoremap <silent> <C-S-k> :move-2<cr>
        nnoremap <silent> <C-S-l> >>
        xnoremap <silent> <C-S-h> <gv
        xnoremap <silent> <C-S-j> :move'>+<cr>gv
        xnoremap <silent> <C-S-k> :move-2<cr>gv
        xnoremap <silent> <C-S-l> >gv
        xnoremap < <gv
        xnoremap > >gv
endif


" Mapping to set wrap
nnoremap <M-z> :set wrap!<cr>

" remove trailing spaces
nnoremap <leader>tt :%s/\s\+$//e

" Change current working directory locally and print cwd after that,
" see https://vim.fandom.com/wiki/Set_working_directory_to_the_current_file
nnoremap <silent> <leader>cd :lcd %:p:h<CR>:pwd<CR>

" Mapping time
imap <leader>t <C-R>=strftime('%c')<CR>

nnoremap <leader>nf :set nofoldenable <cr>

" nnoremap <leader>b :ls<cr>:b<space>

command! Todo call funk#todo()
command! -nargs=+ -complete=command TabMessage call funk#TabMessage(<q-args>)

nnoremap <leader>? :call    funk#goog(expand("<cWORD>"), 0)<cr>
nnoremap <leader>! :call    funk#goog(expand("<cWORD>"), 1)<cr>
xnoremap <leader>? "gy:call funk#goog(@g, 0)<cr>gv
xnoremap <leader>! "gy:call funk#goog(@g, 1)<cr>gv

nnoremap <leader>te :call funk#transl(expand("<cWORD>"), 0)<cr>
xnoremap <leader>te "gy:call funk#transl(@g, 0)<cr>gv

command! ZoomToggle call funk#ZoomToggle()
nnoremap <C-w>z :ZoomToggle<CR>

nmap ,o :call funk#OpenFileInPrevWindow()<CR>

" nnoremap gd :call funk#GoToDefinition()<CR>

nnoremap <leader>f za

" nnoremap <leader>h :call funk#ToggleHiddenAll()<CR>

nnoremap vv V

nnoremap <Leader>pi :PlugInstall<CR>
nnoremap <Leader>pc :PlugClean<CR>
nnoremap <Leader>pu :PlugUpdate<CR>

inoremap <c-a> <c-o>I
inoremap <c-b> <c-o><Left>
inoremap <c-d> <c-o><Del>
inoremap <c-e> <c-o>A
inoremap <c-f> <c-o><Right>
inoremap <c-h> <c-o><BS>

cnoremap <c-a> <HOME>
cnoremap <c-b> <Left>
cnoremap <c-d> <Del>
cnoremap <c-e> <END>
cnoremap <c-f> <Right>
cnoremap <c-h> <BS>

nnoremap <leader>i :exe 'normal! =ip'<CR>

nnoremap <leader>hd :call funk#HeaderComment(getline('.'))<CR>

nnoremap ]b :bnext<CR>
nnoremap [b :bprev<CR>

nmap <silent> gx :!xdg-open <cWORD><cr>

nmap <space>v viw
