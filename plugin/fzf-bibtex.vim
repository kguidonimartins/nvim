function! s:bibtex_cite_sink_insert(lines)
    let r=system("bibtex-cite ", a:lines)
    execute ':normal! a' . r
    call feedkeys('a', 'n')
endfunction

nnoremap <silent> <leader>c :call fzf#run({
            \ 'source': funk#Bibtex_ls(),
            \ 'sink*': function('<sid>bibtex_cite_sink_insert'),
            \ 'down': '50%',
            \ 'options': '--ansi --layout=reverse-list --multi --prompt "Cite> "'})<CR>

inoremap <silent> @@ <c-g>u<c-o>:call fzf#run({
            \ 'source': funk#Bibtex_ls(),
            \ 'sink*': function('<sid>bibtex_cite_sink_insert'),
            \ 'down': '50%',
            \ 'options': '--ansi --layout=reverse-list --multi --prompt "Cite> "'})<CR>
