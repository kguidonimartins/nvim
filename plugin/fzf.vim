"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'junegunn/fzf.vim'                             " find things easy
"------------------------------------------------------------------------------------------------------------------------------------
let g:fzf_layout = { 'window': { 'width': 1, 'height': 0.3, 'yoffset': 0, 'border': 'bottom' } }
let g:fzf_layout = { 'down': '30%' }

" Open FZF search in vim
nnoremap <C-p> :Files<CR>
nnoremap <c-]> :call FzfTagsCurrentWord()<cr>

nnoremap <leader>rg :Rg!<CR>
nnoremap <leader>ra :RGA!<CR>
nnoremap <leader>rw :call FzfCurrentWord()<cr>
nnoremap <leader>b :Buffers<CR>

nnoremap <silent> <leader>df :call fzf#run({
                        \ 'source': 'git --git-dir="${DOTBARE_DIR}" --work-tree="${DOTBARE_TREE}" ls-files --full-name --directory "${DOTBARE_TREE}"',
                        \  'dir': '$HOME',
                        \  'sink': 'e',
                        \  'down': '50%',
                        \  'options': '--multi --prompt "Choose a dotfile to edit> "'})<CR>

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-h': 'split',
  \ 'ctrl-v': 'vsplit'
  \ }

function! FzfTagsCurrentWord()
        let l:word = expand('<cword>')
        let l:list = taglist(l:word)
        if len(l:list) == 1
                execute ':tag ' . l:word
        else
                call fzf#vim#tags(l:word)
        endif
endfunction

function! FzfCurrentWord()
        let l:word = expand('<cword>')
        execute ':RGA ' . l:word
endfunction

command! -bang -nargs=* RGA
  \ call fzf#vim#grep(
  \   'rga --column --line-number --hidden --ignore-case --no-heading --color=always '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview({'options': '--delimiter : --nth 4..'}, 'right:50%')
  \           : fzf#vim#with_preview({'options': '--delimiter : --nth 4..'}, 'right:50%:hidden', '?'),
  \   <bang>0)

function! s:buflist()
  redir => ls
  silent ls
  redir END
  return split(ls, '\n')
endfunction

function! s:bufopen(e)
  execute 'buffer' matchstr(a:e, '^[ 0-9]*')
endfunction

nnoremap <silent> <Leader><Enter> :call fzf#run({
\   'source':  reverse(<sid>buflist()),
\   'sink':    function('<sid>bufopen'),
\   'options': '+m',
\   'down':    len(<sid>buflist()) + 2
\ })<CR>

command! FZFMru call fzf#run({
\ 'source':  reverse(s:all_files()),
\ 'sink':    'edit',
\ 'options': '-m -x +s',
\ 'down':    '40%' })

function! s:all_files()
  return extend(
  \ filter(copy(v:oldfiles),
  \        "v:val !~ 'fugitive:\\|NERD_tree\\|^/tmp/\\|.git/'"),
  \ map(filter(range(1, bufnr('$')), 'buflisted(v:val)'), 'bufname(v:val)'))
endfunction

function! s:line_handler(l)
  let keys = split(a:l, ':\t')
  exec 'buf' keys[0]
  exec keys[1]
  normal! ^zz
endfunction

function! s:buffer_lines()
  let res = []
  for b in filter(range(1, bufnr('$')), 'buflisted(v:val)')
    call extend(res, map(getbufline(b,0,"$"), 'b . ":\t" . (v:key + 1) . ":\t" . v:val '))
  endfor
  return res
endfunction

command! FZFLines call fzf#run({
\   'source':  <sid>buffer_lines(),
\   'sink':    function('<sid>line_handler'),
\   'options': '--extended --nth=3..',
\   'down':    '60%'
\})

" fzf-filemru
" nnoremap <C-p> :ProjectMru<CR>


