" set shiftwidth=4
" set softtabstop=4
" set tabstop=4
" set wildmode=longest,list,full

set autoindent
set autoread
set autowrite
set backspace=indent,eol,start
set clipboard=unnamed,unnamedplus
set completeopt=longest,menuone,preview
set conceallevel=0
set encoding=utf-8
set expandtab
set fileencoding=utf-8
set foldtext=funk#FunkFoldText()
set go=a
set hidden
set history=5000
" set iskeyword+=_,$,@,%,#,-
set list
set modeline
set mouse=a
set nocompatible
set path=.,**
set scrolloff=1
set showcmd
set showmatch
set showtabline=2
set splitright splitbelow
set tags+=./tags;,tags;./md_tags;,md_tags;/.tags;,.tags;
set timeoutlen=500
set title
set wildmenu

" Local project configs
" Use a .exrc (not a .vimrc/.nvimrc) file for specific projects
" Be careful!
set exrc
set secure
