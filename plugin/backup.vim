
silent !mkdir -p ~/.local/share/nvim/undo > /dev/null 2>&1
silent !mkdir -p ~/.local/share/nvim/swap > /dev/null 2>&1
silent !mkdir -p ~/.local/share/nvim/backup > /dev/null 2>&1
set undofile
set swapfile
set backup
set writebackup
set backupcopy=yes
set undodir=~/.local/share/nvim/undo//
set directory=~/.local/share/nvim/swap//
set backupdir=~/.local/share/nvim/backup//

au BufWritePre * let &backupext ='@'.substitute(substitute(substitute(expand('%:p:h'), '/', '%', 'g'), '\', '%', 'g'),  ':', '', 'g').'@'.strftime("%F@%H:%M:%S").'~'

