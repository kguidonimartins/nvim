" select text inside a Rmd block and send to the console via slime plugin
let @a = "?```{r\<CR>f}2wv/```\<CR>b\<c-c>\<c-c>"

" select text inside a Rmd block, send to the console via slime plugin,
" and jump to the next Rmd block
let @b = "?```{r\<CR>f}2wv/```\<CR>b\<c-c>\<c-c>/```{r\<CR>f}2wzz:nohlsearch"





