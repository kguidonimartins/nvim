"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'sirver/UltiSnips'                             " access snippets
"------------------------------------------------------------------------------------------------------------------------------------
let g:UltiSnipsSnippetDirectories=["UltiSnips", "snippets", "/home/karlo/.config/nvim/snippets"]
let g:UltiSnipsListSnippets="<c-i>"
let g:UltiSnipsExpandTrigger="<c-s>"
let g:UltiSnipsEditSplit="tabdo"

