"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'neoclide/coc.nvim'                            " language server
"------------------------------------------------------------------------------------------------------------------------------------
" Disable autostart. Use `CocStart`
" Start coc service on startup, use |CocStart| to start server when you set it to 0.
let g:coc_start_at_startup = 0

if !has('nvim-0.5')
  nnoremap <leader>ls :AutoComplPopDisable \| CocStart<CR>

  let g:coc_global_extensions = [
        \ 'coc-cmake',
        \ 'coc-docker',
        \ 'coc-explorer',
        \ 'coc-highlight',
        \ 'coc-html',
        \ 'coc-json',
        \ 'coc-markdownlint',
        \ 'coc-marketplace',
        \ 'coc-python',
        \ 'coc-r-lsp',
        \ 'coc-sh',
        \ 'coc-snippets',
        \ 'coc-syntax',
        \ 'coc-ultisnips',
        \ 'coc-vimlsp',
        \ 'coc-yaml',
        \ 'coc-yank',
        \ 'coc-bibtex',
        \ 'coc-browser'
        \ ]

  " TODO: o repositório coc-python foi arquivado e a autor sugere usar um dos dois abaixo.
  " \ 'coc-pyls',
  " \ 'coc-pyright',

  " " " Some servers have issues with backup files, see #649.
  " set nobackup
  " set nowritebackup

  " Use <c-space> to trigger completion.
  if has('nvim')
    inoremap <silent><expr> <c-space> coc#refresh()
  else
    inoremap <silent><expr> <c-@> coc#refresh()
  endif

  " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
  " position. Coc only does snippet and additional edit on confirm.
  " <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
  if exists('*complete_info')
    inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
  else
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
  endif

  " map coc explorer
  nmap <leader>t :CocCommand explorer --toggle --sources=buffer+,file+<CR>

  " function! s:show_documentation()
  "   if (index(['vim','help'], &filetype) >= 0)
  "     execute 'h '.expand('<cword>')
  "   elseif (coc#rpc#ready())
  "     call CocActionAsync('doHover')
  "   else
  "     execute '!' . &keywordprg . " " . expand('<cword>')
  "   endif
  " endfunction

  " nnoremap <silent> K :call <SID>show_documentation()<CR>

  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')

  let g:coc_snippet_next = '<tab>'

  nnoremap <C-s> :CocList snippets<CR>

  " " hint for coc-bibtex
  " call coc#config('list.source.bibtex', {
  "   \  'files': [
  "   \    'path/to/my/bibliography.bib'
  "   \  ]
  "   \})

endif
