" general
iabbrev ligth light
iabbrev heigth height
iabbrev lenght length
iabbrev widht width
iabbrev drougth drought

" R
autocmd FileType R,r,Rmd,rmd :iabbrev rr if (!require("tidyverse")) install.packages("tidyverse")<CR>if (!require("here")) install.packages("here")<ESC><CR>
autocmd FileType R,r,Rmd,rmd :iabbrev rm if (!require("remotes")) install.packages("remotes")<CR>if (!require("package")) install_github("user/package")<ESC><CR>
autocmd FileType R,r,Rmd,rmd :iabbrev pp if (interactive()) {<CR> if (!require("httpgd")) install.packages("httpgd")<CR>httpgd::hgd()<CR> httpgd::hgd_browse(browser = "qutebrowser")<CR> }<ESC>
autocmd FileType R,r,Rmd,rmd :iabbrev ppk if (!requireNamespace("pak")) install.packages("pak", repos = "https://r-lib.github.io/p/pak/dev/")<ESC>
