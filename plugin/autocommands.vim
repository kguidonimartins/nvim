autocmd BufWritePost ~/.config/nvim/* silent exe "!/usr/bin/tree -I '~/.config/nvim/.git/*' -L 5 ~/.config/nvim ~/.config/nvim/README"

autocmd BufWritePost ~/.local/suckless/dwmblocks/config.h !cd ~/.local/suckless/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }

autocmd BufWritePost ~/.local/bin/tools/* silent exe "!/usr/bin/tree -L 5 -a ~/.local/bin/tools > ~/.local/bin/tools/STRUCTURE"

autocmd FileType Cpp,cpp sleep 1 | :LspStop
autocmd FileType C,c sleep 1 | :LspStop

" Automatically reload upon save
" https://stackoverflow.com/a/39294493/5974372
" check the system messages with: `:messages`
if has ('autocmd') " Remain compatible with earlier versions
    augroup vimrc
        autocmd! BufWritePost $MYVIMRC source % | echom "Reloaded " . $MYVIMRC | redraw
        autocmd! BufWritePost $MYVIMRC nested source $MYVIMRC | echom "Reloaded " . $MYVIMRC | redraw
    augroup END
endif

" Remember cursor position between vim sessions
" When this does not work, use :wshada
autocmd BufReadPost *
            \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
            \ |   exe "normal! g`\""
            \ | endif

" Open any file with a pre-existing swapfile in readonly mode
augroup NoSimultaneousEdits
    autocmd!
    autocmd SwapExists * let v:swapchoice = 'o'
    autocmd SwapExists * echomsg ErrorMsg
    autocmd SwapExists * echo 'Duplicate edit session (readonly)'
    autocmd SwapExists * echohl None
    autocmd SwapExists * sleep 2
augroup END

" autocmd BufWritePre,FileWritePre,FileAppendPre,FilterWritePre * :call funk#StripTrailingWhitespaces()

" " Make inner change text motions extendeded (*nixcasts)
" let items = [ "<bar>", "\\", "/", ":", ".", "*", "_" ]
" for item in items
"    exe "nnoremap yi".item." T".item."yt".item
"    exe "nnoremap ya".item." F".item."yf".item
"    exe "nnoremap ci".item." T".item."ct".item
"    exe "nnoremap ca".item." F".item."cf".item
"    exe "nnoremap di".item." T".item."dt".item
"    exe "nnoremap da".item." F".item."df".item
"    exe "nnoremap vi".item." T".item."vt".item
"    exe "nnoremap va".item." F".item."vf".item
" endfor

" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Run xrdb whenever Xdefaults or Xresources are updated.
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %

" Run dunst
autocmd BufWritePost *dunstrc !killall dunst && dunst & notify-send "testing modifications" &

" Runs a script that cleans out tex build files whenever I close out of a .tex file.
autocmd VimLeave *.tex !texclear %

" Ensure files are read as what I want:
autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
autocmd BufRead,BufNewFile *.tex set filetype=tex

" Open files with intended programs not vim

augroup nonvim
    au!
    au BufRead *.png,*.jpg,*.gif,*.xls*,*.scpt,*.tiff,*.ppt*,*.doc*,*.rtf,*.docx,*.pdf exe "!xdg-open " . shellescape(expand("%:p")) . "</dev/null >/dev/null 2>&1 & disown" | bd | let &ft=&ft | redraw!
augroup end

autocmd BufRead,BufNewFile * syntax sync fromstart

" autocmd BufWritePost * GitGutter
