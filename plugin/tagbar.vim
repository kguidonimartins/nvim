"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'majutsushi/tagbar'                            " find tags
"------------------------------------------------------------------------------------------------------------------------------------
nmap <F8> :TagbarToggle<CR>
" for bib files
let g:tagbar_type_bib = {
            \ 'ctagstype' : 'bib',
            \ 'kinds'     : [
            \ 'a:Articles',
            \ 'b:Books',
            \ 'L:Booklets',
            \ 'c:Conferences',
            \ 'B:Inbook',
            \ 'C:Incollection',
            \ 'P:Inproceedings',
            \ 'm:Manuals',
            \ 'T:Masterstheses',
            \ 'M:Misc',
            \ 't:Phdtheses',
            \ 'p:Proceedings',
            \ 'r:Techreports',
            \ 'u:Unpublished',
            \ ]
            \ }

" for R files
let g:tagbar_type_r = {
            \ 'ctagstype' : 'r',
            \ 'kinds'     : [
            \ 'f:Functions',
            \ 'g:GlobalVariables',
            \ 'v:FunctionVariables',
            \ ]
            \ }

" for julia files
let g:tagbar_type_julia = {
            \ 'ctagstype' : 'julia',
            \ 'kinds'     : [
            \ 't:struct', 'f:function', 'm:macro', 'c:const']
            \ }

" for makefiles
let g:tagbar_type_make = {
            \ 'kinds':[
            \ 'm:macros',
            \ 't:targets'
            \ ]
            \}

" for markdown files
" let g:tagbar_type_markdown = {
"     \ 'ctagstype' : 'markdown',
"     \ 'kinds' : [
"         \ 'h:Heading_L1',
"         \ 'i:Heading_L2',
"         \ 'k:Heading_L3'
"     \ ]
" \ }

" Add support for markdown files in tagbar.
" see: https://github.com/khughitt/dotfiles
let g:tagbar_type_markdown = {
            \ 'ctagstype': 'markdown',
            \ 'ctagsbin' : '~/.local/bin/tools/vim/markdown2ctags.py',
            \ 'ctagsargs' : '-f - --sort=yes --sro=»',
            \ 'kinds' : [
            \ 's:sections',
            \ 'i:images'
            \ ],
            \ 'sro' : '»',
            \ 'kind2scope' : {
            \ 's' : 'section',
            \ },
            \ 'sort': 0,
            \ }

" for vimwiki
let g:tagbar_type_vimwiki = {
            \   'ctagstype':'vimwiki'
            \ , 'kinds':['h:header']
            \ , 'sro':'&&&'
            \ , 'kind2scope':{'h':'header'}
            \ , 'sort':0
            \ , 'ctagsbin':'~/.local/bin/tools/vim/vwtags.py'
            \ , 'ctagsargs': 'markdown'
            \ }

" for rmarkdown files
" from: https://maximewack.com/post/tagbar/
let g:tagbar_type_rmd = {
            \   'ctagstype':'rmd'
            \ , 'kinds':['h:header', 'c:chunk', 'f:function', 'v:variable']
            \ , 'sro':'&&&'
            \ , 'kind2scope':{'h':'header', 'c':'chunk'}
            \ , 'sort':0
            \ , 'ctagsbin':'~/.local/bin/tools/vim/rmdtags.py'
            \ , 'ctagsargs': ''
            \ }

let g:tagbar_type_rmarkdown = {
            \   'ctagstype':'rmd'
            \ , 'kinds':['h:header', 'c:chunk', 'f:function', 'v:variable']
            \ , 'sro':'&&&'
            \ , 'kind2scope':{'h':'header', 'c':'chunk'}
            \ , 'sort':0
            \ , 'ctagsbin':'~/.local/bin/tools/vim/rmdtags.py'
            \ , 'ctagsargs': ''
            \ }

