set wrapscan
set ignorecase
set smartcase
set incsearch
set hlsearch

nnoremap <silent><esc> :nohlsearch<CR>

nnoremap n nzz
nnoremap N Nzz
nnoremap * *Nzz

set inccommand=split

nnoremap S :%s###g<Left><Left><Left>

" AKA multiple cursor. Use dot command to repeat.
nnoremap c* *``cgn
nnoremap c# #``cgN

nnoremap <Leader>x /\<<C-R>=expand('<cword>')<CR>\>\C<CR>``cgn
nnoremap <Leader>X ?\<<C-R>=expand('<cword>')<CR>\>\C<CR>``cgN
