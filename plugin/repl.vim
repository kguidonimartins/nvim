" NOTE: Another nice REPLs like the vim-slime
" https://github.com/michaelb/sniprun
" https://github.com/hanschen/vim-ipython-cell
" https://github.com/metakirby5/codi.vim

let g:slime_paste_file = "/tmp/slime_paste"
let g:slime_no_mappings = 1
let g:slime_python_ipython = 1

xmap <c-c><c-c> <Plug>SlimeRegionSend
nmap <c-c><c-c> <Plug>SlimeParagraphSend
nmap <c-c>v   <Plug>SlimeConfig

" https://github.com/jpalardy/vim-slime/issues/250
nmap <leader><leader> <c-c><c-c>}jzz
nmap <Space><Space> <c-c><c-c>}jzz
nmap <leader><Space> <c-c><c-c>
nmap <leader>a 0Vf{%
nmap <leader>ew viw<c-c><c-c>

" The default is tmux, but you can use these function to switch between X11 and tmux!
" Use with: `:call Slime<Tab>`
function SlimeX11()
  let g:slime_target = "X11"
endfunction

function SlimeTmux()
        let g:slime_target = "tmux"
        let g:slime_default_config = {
                \ "socket_name": get(split($TMUX, ','), 0),
                \ "target_pane": "{top-left}"
                \ }
        let g:slime_dont_ask_default = 0
endfunction

" Set terminal inside tmux as default
let g:slime_target = "tmux"
let g:slime_default_config = {
        \ "socket_name": get(split($TMUX, ','), 0),
        \ "target_pane": "{top-left}"
        \ }
let g:slime_dont_ask_default = 1

" " This solve the problem of using slime with radian!
" " Please, see:
" " - https://github.com/randy3k/radian/issues/114
" " - https://github.com/jpalardy/vim-slime/issues/211
" " - https://github.com/slowkow/dotfiles/blob/275d3b790ad1b3fc5a5eca95110d9cf73e604477/.vimrc#L85

" function! _EscapeText_r(text)
"   call system("cat > " . "/tmp/slime_r", a:text)
"   return ["source('/tmp/slime_r', echo = TRUE, max.deparse.length = 4095)\r"]
" endfunction

" function! _EscapeText_rmd(text)
"   call system("cat > " . "/tmp/slime_r", a:text)
"   return ["source('/tmp/slime_r', echo = TRUE, max.deparse.length = 4095)\r"]
" endfunction

" function! _EscapeText_rmarkdown(text)
"   call system("cat > " . "/tmp/slime_r", a:text)
"   return ["source('/tmp/slime_r', echo = TRUE, max.deparse.length = 4095)\r"]
" endfunction

" jupyter  ---------------------------------------------------------------------
"# Adapted from: https://www.blog.gambitaccepted.com/2020/04/26/neovim-qtconsole-setup/

" NOTE: work only for python
function! GetKernel()
        let kernel = system('echo "ipykernel_$(basename "$(pwd)")" | tr -d "\n"')
        return kernel
endfunction

function! RunQtConsole()
        let l:kernel = GetKernel()
        call jobstart(["jupyter", "qtconsole", "--JupyterWidget.include_other_output=True", "--kernel", l:kernel])
endfunction


function! ConnectToPipenvKernel()
        let l:kernel = GetKernel()
        call IPyConnect('--kernel', l:kernel, '--no-window', '--existing')
endfunction

let g:ipy_celldef = '^##' " regex for cell start and end

nmap <silent> <leader>jqt :call RunQtConsole()<CR>
nmap <silent> <leader>jk :call ConnectToPipenvKernel()<CR>
nmap <silent> <leader>jc <Plug>(IPy-RunCell)
nmap <silent> <leader>ja <Plug>(IPy-RunAll)

" NOTE: If you want to R in a jupyter-qtconsole, just run:
" First, remember to install:
" python -m ipykernel install --user
" Rscript -e 'IRkernel::installspec()'
" then:
" :call jobstart(["jupyter", "qtconsole", "--JupyterWidget.include_other_output=True", "--kernel", "r"])
" then:
" :call SlimeX11()
" then:
" C-c C-c
" then:
" click on the jupyter-qtconsole window
