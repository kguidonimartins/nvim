" 0: never shown
" 1: shown when there's more than one tab or buffer open
" 2: always shown
let g:vem_tabline_show=1

" none: no number is shown
" buffnr: Vim's buffer number is shown
" index: displayed buffers are numbered sequentially starting from 1
let g:vem_tabline_show_number='none'

let g:vem_tabline_number_symbol=' '
