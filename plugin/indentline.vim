" Configs for
 " 'kguidonimartins/indentLine'
 " 'lukas-reineke/indent-blankline.nvim'
let g:indent_blankline_char = '¦'
let g:indentLine_enabled = 1
let g:indent_blankline_debug = v:true
let g:indent_blankline_filetype_exclude = ['help', 'term', 'terminal', 'fzf']
let g:indent_blankline_enabled = v:true
let g:indent_blankline_buftype_exclude = ['terminal']
