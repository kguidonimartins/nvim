"  Vim inside tmux

" cursor shape
if exists('$TMUX')
    let &t_SI = "\<Esc>Ptmux;\<Esc>\e[5 q\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\e[2 q\<Esc>\\"
else
    let &t_SI = "\e[5 q"
    let &t_EI = "\e[2 q"
endif

" from: https://thoughtbot.com/upcase/videos/tmux-vim-integration#layout-balancing
" automatically rebalance windows on vim resize
autocmd VimResized * :wincmd =

" Reload
au FocusGained,BufEnter * :checktime

" from: https://thoughtbot.com/upcase/videos/tmux-vim-integration#layout-balancing
" zoom a vim pane, <C-w>= to re-balance
nnoremap <leader>- :wincmd _<cr>:wincmd \|<cr>
nnoremap <leader>= :wincmd =<cr>

" Rename tmux tabs based on opened file
" https://vi.stackexchange.com/a/19062/29076
augroup TMUX
  autocmd!
  if exists('$TMUX')
    autocmd BufReadPost,FileReadPost,BufNewFile,FocusGained * call system("tmux rename-window " . expand("%:t:r"))
    autocmd VimLeave,FocusLost * call system("tmux set-window-option automatic-rename")
  endif
augroup END


function! RunRREPL()
        call system("tmux splitw -hb -p 35 -c '#{pane_current_path}' 'bash -c rr'; tmux select-pane -l")
endfunction

function! RunIPyREPL()
        call system("tmux splitw -hb -p 35 -c '#{pane_current_path}' 'bash -c ipython'; tmux select-pane -l")
endfunction

nnoremap <leader>` :call RunRREPL()<CR>
nnoremap <leader>~ :call RunIPyREPL()<CR>
