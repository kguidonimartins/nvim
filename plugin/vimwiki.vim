"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'vimwiki/vimwiki', { 'branch': 'dev' }         " vimwiki syntax
"------------------------------------------------------------------------------------------------------------------------------------
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
let g:vimwiki_global_ext = 0
map <leader>v :VimwikiIndex
" TODO: implement others wikis
let g:vimwiki_list = [
                       \ {'path': '~/google-drive/kguidonimartins/git-repos/vimwiki/docs', 'syntax': 'markdown', 'ext': '.md', 'auto_tags': 1}
                     \]
nmap <Space>l :r ~/.config/nvim/templates/markdown-yml.md<CR>
nmap <Leader>lw :Files ~/google-drive/kguidonimartins/git-repos/vimwiki/docs<CR>

function! VimwikiLinkHandler(link)
	" Use Vim to open external files with the 'vfile:' scheme.  E.g.:
	"   1) [[vfile:~/Code/PythonProject/abc123.py]]
	"   2) [[vfile:./|Wiki Home]]
	let link = a:link
	if link =~# '^vfile:'
		let link = link[1:]
	else
		return 0
	endif
	let link_infos = vimwiki#base#resolve_link(link)
	if link_infos.filename == ''
		echomsg 'Vimwiki Error: Unable to resolve link!'
		return 0
	else
		exe 'vsplit ' . fnameescape(link_infos.filename)
		return 1
	endif
endfunction


