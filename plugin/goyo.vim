"------------------------------------------------------------------------------------------------------------------------------------
" Plug 'junegunn/goyo.vim'                            " focus mode
"------------------------------------------------------------------------------------------------------------------------------------
" map <leader>f :Goyo \| set bg=light \| set linebreak<CR>

let g:goyo_width=90

" map <leader>g :Goyo<CR>

function! s:goyo_enter()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status off
    silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  endif
  set noshowmode
  set noshowcmd
  set scrolloff=999
  set linebreak
  set list!
  let g:vem_tabline_show = 0
  " Limelight
  " set cursorline!
  " hi   NonText ctermfg=239 gui=NONE guifg=#343746
  " hi link MsgSeparator NonText
  " hi link ColorColumn NonText
  " hi link Whitespace NonText
  " hi link SignColumn NonText
  " hi link FoldColumn NonText
  " hi link StatusLine NonText
  " hi link StatusLineNC NonText
  " hi link VertSplit NonText
endfunction

function! s:goyo_leave()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status on
    silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
  endif
  set showmode
  set showcmd
  set scrolloff=5
  let g:vem_tabline_show = 1
  syntax on
  " Limelight!
  autocmd ColorScheme * call MyHighlights()
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

" Color name (:help cterm-colors) or ANSI code
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240

" Color name (:help gui-colors) or RGB color
let g:limelight_conceal_guifg = 'DarkGray'
let g:limelight_conceal_guifg = '#777777'
