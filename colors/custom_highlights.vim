" Use the below command to sort cterm* and gui* arguments horizontally.
" Using 0AAhi 0aGroup works.
" :call setline(line('.'),join(sort(split(getline('.'))), ' '))

" transparent background

" #FFFFFF
" #eeeeee
" #eeeeee
" #b2b2b2
" guibg=NONE
hi   Normal       cterm=NONE     ctermbg=NONE   ctermfg=249    gui=NONE   guibg=#132738   guifg=#eeeeee
hi   LineNr       cterm=NONE     ctermbg=NONE   guibg=NONE
hi   SignColumn   cterm=NONE     ctermbg=NONE   ctermfg=NONE   guibg=NONE    guifg=NONE

" remove
hi   clear   ALESignColumnWithErrors
hi   clear   FoldColumn
hi   clear   SignColumn
hi   clear   ColorColumn
hi   clear   MsgSeparator
hi   clear   Whitespace
hi   clear   markdownError

" link
hi   link    rConditional             Repeat
hi   link    pandocCiteKey            String
hi   link    rmdYamlFieldTtl          Function
hi   link    rmdInlineDelim           String
hi   link    rmdrInline               String
hi   link    vimHiGroup               VimGroup

" custom highlights
hi   ALEErrorSign             gui=bold        guibg=NONE      guifg=#FF5555
hi   ALEInfoSign              gui=bold        guibg=NONE      guifg=#edc809
hi   ALEWarningSign           gui=bold        guibg=NONE      guifg=#edc809
hi   Blamer                   gui=NONE        guifg=#343746
hi   Comment                  ctermfg=239     gui=NONE        guifg=#544a65
hi   Conceal                  cterm=NONE      ctermbg=NONE    ctermfg=239     gui=NONE        guibg=NONE      guifg=#343746
hi   CursorColumn             cterm=NONE      ctermbg=NONE    gui=NONE        guibg=NONE
hi   CursorLineNr             cterm=NONE      ctermbg=NONE    gui=NONE        guibg=NONE
hi   Delimiter                guifg=#fe8019
hi   Error                    ctermfg=253     ctermbg=203     guifg=#F8F8F2   guibg=#FF5555
hi   ErrorMsg                 cterm=bold      ctermfg=253     ctermbg=203     gui=bold        guifg=#F8F8F2   guibg=#FF5555
hi   ExtraWhitespace          ctermbg=red     guibg=#FF5555
hi   FileChanged              cterm=bold      ctermbg=238     ctermfg=15      gui=NONE        guibg=#FF5555       guifg=#eeeeee
hi   FileUnchanged            cterm=bold      ctermbg=238     ctermfg=15      gui=NONE        guibg=#2A265A   guifg=#eeeeee
hi   FoldColumn               cterm=NONE      ctermbg=NONE    ctermfg=NONE    gui=NONE        guibg=NONE      guifg=NONE
hi   Folded                   ctermbg=235     ctermfg=61      guibg=#21222C   guifg=#6272A4
hi   Function                 cterm=bold      ctermfg=170     gui=bold        guifg=#bc6ec5
hi   IncSearch                cterm=NONE      ctermbg=NONE    ctermfg=239     gui=NONE        guibg=#edc809    guifg=#FF5555
hi   InsertUnchanged          cterm=bold      ctermbg=238     ctermfg=15      gui=NONE        guibg=#eeeeee   guifg=#424450
hi   markdownBold             cterm=bold      ctermbg=NONE    ctermfg=249     gui=bold        guibg=NONE      guifg=#eeeeee
hi   markdownBoldDelimiter    cterm=bold      ctermbg=NONE    ctermfg=249     gui=bold        guibg=NONE      guifg=#eeeeee
hi   markdownCode             ctermfg=133 guifg=#a45bad
hi   markdownCodeBlock        ctermfg=133 guifg=#a45bad
hi   markdownError            cterm=bold      ctermfg=170     gui=NONE        guibg=NONE      guifg=NONE
hi   markdownH1               cterm=bold      ctermfg=170     gui=bold        guifg=#bc6ec5
hi   markdownH2               cterm=bold      ctermfg=170     gui=bold        guifg=#bc6ec5
hi   markdownH3               cterm=bold      ctermfg=170     gui=bold        guifg=#bc6ec5
hi   markdownH4               cterm=bold      ctermfg=170     gui=bold        guifg=#bc6ec5
hi   markdownH5               cterm=bold      ctermfg=170     gui=bold        guifg=#bc6ec5
hi   markdownH6               cterm=bold      ctermfg=170     gui=bold        guifg=#bc6ec5
hi   markdownHeadingDelimiter ctermfg=228     gui=bold        guifg=#F1FA8C
hi   NonText                  ctermfg=239     gui=NONE        guifg=#343746
hi   pandocHTMLCommentEnd     ctermfg=239     guifg=#343746
hi   pandocHTMLCommentStart   ctermfg=239     guifg=#343746
hi   rDollar                  cterm=NONE      ctermfg=212     gui=NONE        guifg=#FF79C6
hi   Repeat                   cterm=bold      ctermfg=168     gui=bold        guifg=#ce537a
hi   rOperator                cterm=NONE      ctermfg=212     gui=NONE        guifg=#FF79C6
hi   Search                   cterm=NONE      ctermbg=blue    ctermfg=grey    gui=bold        guibg=#FF5555       guifg=#edc809
hi   Statement                cterm=NONE      ctermfg=212     gui=NONE        guifg=#FF79C6
hi   StatusLine               cterm=bold      ctermbg=238     ctermfg=15      gui=bold        guibg=#2A265A
hi   StatusLineNC             cterm=NONE      ctermbg=238     ctermfg=15      gui=NONE        guibg=#2A265A
hi   StatusLineTerm           cterm=bold      ctermbg=238     gui=bold        guibg=#2A265A
hi   StatusLineTermNC         ctermbg=237     guibg=#2A265A
hi   String                   ctermfg=228     gui=NONE        guifg=#F1FA8C
hi   TabLine                  cterm=NONE      ctermbg=234     ctermfg=60      gui=NONE        guibg=#212026   guifg=#544a65
hi   TabLineFill              cterm=NONE      ctermbg=234     ctermfg=60      gui=NONE        guibg=#212026   guifg=#544a65
hi   TabLineSel               cterm=bold      ctermbg=234     ctermfg=70      gui=bold        guibg=#212026   guifg=#eeeeee
hi   TODO                     cterm=NONE      ctermbg=blue    ctermfg=grey    gui=bold        guibg=#FF5555       guifg=#edc809
hi   User1                    cterm=bold      ctermbg=238     ctermfg=15      gui=bold        guibg=#2A265A   guifg=#eeeeee
hi   User2                    cterm=NONE      ctermbg=238     ctermfg=15      gui=NONE        guibg=#2A265A   guifg=#eeeeee
hi   User3                    cterm=bold      ctermbg=238     ctermfg=15      gui=bold        guibg=#edc809    guifg=#FF5555
hi   User4                    cterm=NONE      ctermbg=238     ctermfg=15      gui=NONE        guibg=#2A265A    guifg=NONE
hi   YankedText               ctermfg=239     gui=NONE        guibg=NONE      guifg=#edc809
hi   VertSplit                cterm=NONE      ctermbg=NONE    gui=NONE        guibg=NONE
hi   Visual                   cterm=NONE      ctermbg=60      ctermfg=white   gui=NONE        guibg=#544a65   guifg=#F8F8F2
hi   WarningMsg               cterm=bold      ctermfg=11      gui=bold        guifg=#edc809

hi LspDiagnosticsDefaultError gui=undercurl guifg=#FF5555
hi LspDiagnosticsDefaultHint gui=undercurl guifg=#edc809
hi LspDiagnosticsDefaultInformation gui=undercurl guifg=green
hi LspDiagnosticsDefaultWarning gui=undercurl guifg=blue

hi LspDiagnosticsFloatingError gui=undercurl guifg=#FF5555
hi LspDiagnosticsFloatingHint gui=undercurl guifg=#edc809
hi LspDiagnosticsFloatingInformation gui=undercurl guifg=green
hi LspDiagnosticsFloatingWarning gui=undercurl guifg=blue

hi LspDiagnosticsUnderlineError gui=undercurl guifg=#FF5555
hi LspDiagnosticsUnderlineHint gui=undercurl guifg=#edc809
hi LspDiagnosticsUnderlineInformation gui=undercurl guifg=green
hi LspDiagnosticsUnderlineWarning gui=undercurl guifg=blue

hi LspDiagnosticsVirtualTextError gui=undercurl guifg=#FF5555
hi LspDiagnosticsVirtualTextHint gui=undercurl guifg=#edc809
hi LspDiagnosticsVirtualTextInformation gui=undercurl guifg=green
hi LspDiagnosticsVirtualTextWarning gui=undercurl guifg=blue

hi LspDiagnosticsSignError gui=bold guifg=#FF5555
hi LspDiagnosticsSignHint gui=bold guifg=#edc809
hi LspDiagnosticsSignInformation gui=bold guifg=green
hi LspDiagnosticsSignWarning gui=bold guifg=blue

highlight link TSError Normal

hi WhichKeyFloat guibg=#343746
