require'bufferline'.setup{
  options = {
    view = "default",
    numbers = "none",
    -- number_style = "none", -- buffer_id at index 1, ordinal at index 2
    -- mappings = false,
    buffer_close_icon= '',
    modified_icon = '●',
    close_icon = '',
    left_trunc_marker = '',
    right_trunc_marker = '',
    max_name_length = 40,
    max_prefix_length = 15, -- prefix used when a buffer is deduplicated
    tab_size = 18,
    diagnostics = false,
    show_buffer_close_icons = false,
    show_buffer_icons = false,
    show_close_icon = false,
    show_tab_indicators = true,
    persist_buffer_sort = true, -- whether or not custom sorted buffers should persist
    -- can also be a table containing 2 custom separators
    -- [focused and unfocused]. eg: { '|', '|' }
    separator_style = false,
    enforce_regular_tabs = false,
    always_show_bufferline = true,
    indicator_icon = '',
    name_formatter = function(buf)  -- buf contains a "name", "path" and "bufnr"
      return buf.path
    end,
  }
}
