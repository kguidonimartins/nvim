local saga = require 'lspsaga'

saga.init_lsp_saga {
        use_saga_diagnostic_sign = true,
        error_sign = '',
        warn_sign = '',
        hint_sign = '',
        infor_sign = '',
        dianostic_header_icon = '   ',
        code_action_icon = ' ',
        code_action_prompt = {
                enable = true,
                sign = true,
                sign_priority = 20,
                virtual_text = true,
        },
        finder_definition_icon = '  ',
        finder_reference_icon = '  ',
        max_preview_lines = 10, -- preview lines of lsp_finder and definition preview
        finder_action_keys = {
                open = 'o',
                vsplit = 'v',
                split = 's',
                quit = 'q',
                scroll_down = '<C-f>',
                scroll_up = '<C-b>' -- quit can be a table
        },
        code_action_keys = {
                quit = 'q',
                exec = '<CR>'
        },
        rename_action_keys = {
                quit = '<C-c>',
                exec = '<CR>'  -- quit can be a table
        },
        definition_preview_icon = '  ',
        border_style = "single",
        rename_prompt_prefix = '➤'
}

-- local lspsaga_mappings = {
--     {'n', 'gh', ":lua require('lspsaga.provider').lsp_finder() <cr>"},
--     {'n', '<space>ca', ":lua require('lspsaga.codeaction').code_action()<CR>"},
--     {'v', '<space>ca', ":lua require('lspsaga.codeaction').range_code_action()<CR>"},
--     {'n', 'K', ":lua require('lspsaga.hover').render_hover_doc()<CR>"},
--     {'n', 'gs', ":lua require('lspsaga.signaturehelp').signature_help()<CR>"},
--     {'n', 'gr', ":lua require('lspsaga.rename').rename()<CR>"},
--     {'n', '<space>gd', ":lua require('lspsaga.provider').preview_definition()<CR>"},
--     {'n', 'cd', ":lua require('lspsaga.diagnostic').show_line_diagnostics()<CR>"},
--     {'n', 'cc', ":lua require('lspsaga.diagnostic').show_cursor_diagnostics()<CR>"},
--     {'n', '[d', ":lua require('lspsaga.diagnostic').lsp_jump_diagnostic_prev()<CR>"},
--     {'n', 'd]', ":lua require('lspsaga.diagnostic').lsp_jump_diagnostic_next()<CR>"}
-- }


vim.cmd[[nnoremap <silent> K <cmd>lua require('lspsaga.hover').render_hover_doc()<CR>]]
