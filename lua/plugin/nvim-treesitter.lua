local ts_config = require("nvim-treesitter.configs")

ts_config.setup {
        ensure_installed = {
                "bash",
                "bibtex",
                "c",
                "clojure",
                "comment",
                "cpp",
                "css",
                "go",
                "graphql",
                "haskell",
                "html",
                "java",
                "javascript",
                "jsdoc",
                "json",
                "jsonc",
                "julia",
                "kotlin",
                "latex",
                "ledger",
                "lua",
                "python",
                "query",
                "r",
                "regex",
                "rst",
                "ruby",
                "rust",
                "scala",
                "typescript",
                "vue",
                "yaml"
        },
        highlight = {
                enable = true,
                use_languagetree = true
        },
        indent = {
                enable = true,
                disable = { "r" }
        },
        incremental_selection = {
                enable = true,
                keymaps = {
                        init_selection = "gnn",
                        node_incremental = "grn",
                        scope_incremental = "grc",
                        node_decremental = "grm",
                },
        },
}


