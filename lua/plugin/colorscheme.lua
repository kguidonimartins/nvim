vim.api.nvim_command('highlight Folded ctermbg=235 ctermfg=61 guibg=#21222C guifg=#6272A4')
vim.api.nvim_command('highlight Function cterm=bold ctermfg=170 gui=bold guifg=#bc6ec5')

vim.cmd[[set termguicolors]]

-- customize dracula color palette
vim.g.dracula_colors = {
  bg = "#132738",
  fg = "#bbbbbb",
  selection = "#44475A",
  comment = "#6272A4",
  red = "#FF5555",
  orange = "#FFB86C",
  yellow = "#F1FA8C",
  green = "#50fa7b",
  purple = "#BD93F9",
  cyan = "#8BE9FD",
  pink = "#FF79C6",
  bright_red = "#FF6E6E",
  bright_green = "#69FF94",
  bright_yellow = "#FFFFA5",
  bright_blue = "#D6ACFF",
  bright_magenta = "#FF92DF",
  bright_cyan = "#bbbbbb",
  bright_white = "#bbbbbb",
  menu = "#21222C",
  visual = "#3E4452",
  gutter_fg = "#4B5263",
  nontext = "#3B4048",
}
-- show the '~' characters after the end of buffers
vim.g.dracula_show_end_of_buffer = true
-- use transparent background
vim.g.dracula_transparent_bg = true
-- set custom lualine background color
vim.g.dracula_lualine_bg_color = "#44475a"
-- set italic comment
vim.g.dracula_italic_comment = true
