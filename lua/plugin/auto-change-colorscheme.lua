vim.cmd[[colorscheme dracula]]
vim.cmd[[set background=dark]]
vim.cmd[[set listchars=tab:▸\ ,eol:¬,nbsp:_,trail:~]]
