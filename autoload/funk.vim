" Todo
function! funk#todo() abort
  let entries = []
  for cmd in ['git grep -niI -e TODO -e FIXME -e XXX -e NOTE -e REVIEW 2> /dev/null',
        \ 'grep -rniI -e TODO -e FIXME -e XXX -e NOTE -e REVIEW * 2> /dev/null']
    let lines = split(system(cmd), '\n')
    if v:shell_error != 0 | continue | endif
    for line in lines
      let [fname, lno, text] = matchlist(line, '^\([^:]*\):\([^:]*\):\(.*\)')[1:3]
      call add(entries, { 'filename': fname, 'lnum': lno, 'text': text })
    endfor
    break
  endfor

  if !empty(entries)
    call setqflist(entries)
    copen
  endif
endfunction

" <Leader>?/! | Google it / Feeling lucky
function! funk#goog(pat, lucky)
  let q = '"'.substitute(a:pat, '["\n]', ' ', 'g').'"'
  let q = substitute(q, '[[:punct:] ]',
        \ '\=printf("%%%02X", char2nr(submatch(0)))', 'g')
  call system(printf('xdg-open "https://www.google.com/search?%sq=%s" </dev/null >/dev/null 2>&1 & disown',
        \ a:lucky ? 'btnI&' : '', q))
endfunction

" See: https://github.com/kguidonimartins/kdotfiles/blob/b78f35027cd6915917a2386a8c2132c752ff33a9/.vimrc#L42
function! funk#ToggleSpell()
  if &spell
    if &spelllang == "pt"
      set spelllang=pt,es,en
      echo "toggle spell" &spelllang
    elseif &spelllang == "pt,es,en"
      set spelllang=en
      echo "toggle spell" &spelllang
    elseif &spelllang == "en"
      set spelllang=es
      echo "toggle spell" &spelllang
    else
      set spell!
      echo "toggle spell off"
    endif
  else
    set spelllang=pt
    set spell!
    echo "toggle spell" &spelllang
  endif
endfunction

" Zoom splits
" Zoom / Restore window.
function! funk#ZoomToggle() abort
  if exists('t:zoomed') && t:zoomed
    execute t:zoom_winrestcmd
    let t:zoomed = 0
  else
    let t:zoom_winrestcmd = winrestcmd()
    resize
    vertical resize
    let t:zoomed = 1
  endif
endfunction

function! funk#OpenFileInPrevWindow()
  let cfile = expand("<cfile>")
  execute "edit " . cfile
endfunction

" bibtex-ls https://github.com/msprev/fzf-bibtex
function! funk#Bibtex_ls()
  " \ globpath('.', '*.bib', v:true, v:true) +
  " \ globpath('..', '*.bib', v:true, v:true) +
  let bibfiles = (
        \ globpath('*/', '*.bib', v:true, v:true) +
        \ globpath('*/*/', '*.bib', v:true, v:true)
        \ )
  let bibfiles = join(bibfiles, ' ')
  let source_cmd = 'bibtex-ls '.bibfiles
  return source_cmd
endfunction

" <Leader>te | Translate
function! funk#transl(pat, lucky)
  let q = ''.substitute(a:pat, '["\n]', ' ', 'g').''
  let q = substitute(q, '[[:punct:] ]',
        \ '\=printf("%%%02X", char2nr(submatch(0)))', 'g')
  call system(printf('xdg-open "https://translate.google.com/?hl=pt-BR#en/pt/%s%s" </dev/null >/dev/null 2>&1 & disown',
        \ a:lucky ? 'btnI&' : '', q))
endfunction

" Quick insert quotes
function! funk#QuickWrap(wrapper)
  let l:w = a:wrapper
  let l:inside_or_around = (&selection == 'exclusive') ? ('i') : ('a')
  normal `>
  execute "normal " . inside_or_around . escape(w, '\')
  normal `<
  execute "normal i" . escape(w, '\')
  normal `<
endfunction

" redirect excommand output
function! funk#TabMessage(cmd)
  redir => message
  silent execute a:cmd
  redir END
  if empty(message)
    echoerr "no output"
  else
    " use "new" instead of "tabnew" below if you prefer split windows instead of tabs
    tabnew
    setlocal buftype=nofile bufhidden=wipe noswapfile nobuflisted nomodified
    silent put=message
  endif
endfunction

function! funk#GoToDefinition()
  if CocAction('jumpDefinition')
    return v:true
  endif
  let ret = execute("silent! normal g]")
  if ret =~ "Error" || ret =~ "Error"
    call searchdecl(expand('<cword>'))
  endif
endfunction

function! funk#StripTrailingWhitespaces()
  if !&binary && &filetype != 'diff'
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
  endif
endfun

function! funk#JumpToNextHeading(direction, count)
  let col = col(".")
  silent execute a:direction == "up" ? '?^#' : '/^#'
  if a:count > 1
    silent execute "normal! " . repeat("n", a:direction == "up" && col != 1 ? a:count : a:count - 1)
  endif
  silent execute "normal! " . col . "|"
  unlet col
endfunction


function! funk#MarkdownLevel()
  let h = matchstr(getline(v:lnum), '^#\+')
  if empty(h)
    return "="
  else
    return ">" . len(h)
  endif
endfunction

function! funk#FunkFoldText()
  let offset = 8
  let line = getline(v:foldstart)
  let line2 = getline(v:foldstart + 1)
  let sub = substitute(line . line2, '```{\|}\|#\|/\*\|\*/\|{{{\d\=\|"', '', 'g')
  let hashcount = count(getline(v:foldstart), "#")
  let rmdchunk = count(getline(v:foldstart), "```{")
  if rmdchunk == 1
    let rmdchunk = 4
  endif
  let ind = indent(v:foldstart)
  let lines = v:foldend-v:foldstart + 1
  let i = 0
  let spaces = ''
  while i < (ind - ind/4)
    let spaces .= ' '
    let i = i+1
  endwhile
  return spaces . repeat(' ', hashcount) . repeat(' ', rmdchunk) . sub . repeat('-', winwidth(0) - strlen(spaces . sub) - offset - hashcount - rmdchunk) . '('. lines .')'
endfunction

" Function for toggling the bottom statusbar:
let s:hidden_all = 1
function! funk#ToggleHiddenAll()
  if s:hidden_all  == 0
    if executable('tmux') && strlen($TMUX)
      silent !tmux set status off
      silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
    endif
    let s:hidden_all = 1
    let g:vem_tabline_show = 0
    set noshowmode
    set noruler
    set laststatus=0
    set noshowcmd
    set showtabline=1
  else
    if executable('tmux') && strlen($TMUX)
      silent !tmux set status on
      silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
    endif
    let s:hidden_all = 0
    let g:vem_tabline_show = 1
    set showmode
    set ruler
    set laststatus=2
    set showcmd
    set showtabline=2
  endif
endfunction

" roxygen2 -----------------------------------------------------------------------------
function! funk#Getparams()
  let s:start = line('.')
  let s:end = search("{")
  if stridx(getline(s:end),"{") == 0
    let s:end = s:end - 1
  endif
  let s:lines=getline(s:start,s:end)
  let linesCnt=len(s:lines)
  let mlines=join(s:lines)
  let mlines=substitute(mlines," ","","g")
  let paraFlag1=stridx(mlines,'(')
  let paraFlag2=strridx(mlines,')')
  let paraLen=paraFlag2-paraFlag1-1
  let parastr=strpart(mlines,paraFlag1+1,paraLen)
  let alist=[]
  if  stridx(parastr,',') != -1
    let s:paras=split(parastr,',')
    let s:idx=0
    while s:idx < len(s:paras)
      if stridx(s:paras[s:idx],'=') != -1
        let s:realpara = split(s:paras[s:idx],'=')[0]
      else
        let s:realpara = s:paras[s:idx]
      endif
      "strip the leading blanks
      "call append(s:start - 1 + s:idx , "#' @param " . s:realpara)
      call add(alist,s:realpara)
      let s:idx = s:idx + 1
    endwhile
  else
    "call append(s:start-1,parastr)
    if parastr != ""
      call add(alist,parastr)
    endif
  endif
  return alist
endfunction

function! funk#Rdoc()
  let s:wd=expand("")
  let s:lineNo=line('.')-1
  let plist=funk#Getparams()
  call append(s:lineNo + 0, "#' Title of my function")
  call append(s:lineNo + 1, "#'")
  call append(s:lineNo + 2, "#' @description")
  call append(s:lineNo + 3, "#' Function description")
  call append(s:lineNo + 4, "#'")
  call append(s:lineNo + 5, "#' @details")
  call append(s:lineNo + 6, "#' Function details")
  call append(s:lineNo + 7, "#'")
  let s:idx =0
  while s:idx < len(plist)
    call append(s:lineNo + 8 + s:idx , "#' @param " . plist[s:idx] . " value")
    let s:idx = s:idx + 1
  endwhile
  call append(s:lineNo + 8 + s:idx + 0, "#'")
  call append(s:lineNo + 8 + s:idx + 1, "#' @return what this function return?")
  call append(s:lineNo + 8 + s:idx + 2, "#'")
  call append(s:lineNo + 8 + s:idx + 3, "#' @importFrom package_name func_of_package")
  call append(s:lineNo + 8 + s:idx + 4, "#'")
  call append(s:lineNo + 8 + s:idx + 5, "#' @section Custom_section_name:")
  call append(s:lineNo + 8 + s:idx + 6, "#'")
  call append(s:lineNo + 8 + s:idx + 7, "#' @export")
  call append(s:lineNo + 8 + s:idx + 8, "#'")
  call append(s:lineNo + 8 + s:idx + 9, "#' @examples")
  call append(s:lineNo + 8 + s:idx + 10, "#' \dontrun{")
  call append(s:lineNo + 8 + s:idx + 11, "#' # put here how your function can be used")
  call append(s:lineNo + 8 + s:idx + 12, "#' }")
endfunction

func! funk#HeaderComment(text)
  let width = 80
  let text = printf("%s ", a:text)
  let chrome = repeat('-', width - strdisplaywidth(text))
  call setline('.', text . chrome)
endfunc

