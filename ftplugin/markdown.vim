setlocal nofoldenable

" markdown : jump to next heading
nnoremap <buffer> <silent> ]] :<C-u>call funk#JumpToNextHeading("down", v:count2)<CR>
nnoremap <buffer> <silent> [[ :<C-u>call funk#JumpToNextHeading("up", v:count1)<CR>

" au BufEnter *.Rmd setlocal foldexpr=funk#MarkdownLevel()
" au BufEnter *.Rmd setlocal foldmethod=
autocmd FileType markdown setlocal foldexpr=StackedRmdFolds()
autocmd FileType markdown setlocal foldmethod=expr
