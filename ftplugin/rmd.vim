setlocal ts=2 sts=2 sw=2 expandtab
setlocal linebreak
autocmd Filetype R,r,rmd,Rmd inoremap ,m <space>%>%<cr>
autocmd Filetype R,r,rmd,Rmd inoremap ,k <space>%>%
autocmd Filetype R,r,rmd,Rmd inoremap <M--> <space><-<space>
autocmd Filetype R,r,rmd,Rmd nnoremap j gj
autocmd Filetype R,r,rmd,Rmd nnoremap k gk


autocmd Filetype rmd,Rmd nnoremap ,va @a
autocmd Filetype rmd,Rmd nnoremap ,vb @b

" markdown : jump to next heading
nnoremap <buffer> <silent> ]] :<C-u>call funk#JumpToNextHeading("down", v:count1)<CR>
nnoremap <buffer> <silent> [[ :<C-u>call funk#JumpToNextHeading("up", v:count1)<CR>


" au BufEnter *.Rmd setlocal foldexpr=funk#MarkdownLevel()
" au BufEnter *.Rmd setlocal foldmethod=
autocmd FileType rmd,Rmd setlocal foldexpr=StackedRmdFolds()
autocmd FileType rmd,Rmd setlocal foldmethod=expr
autocmd FileType rmd,Rmd setlocal foldtext=RmdFoldText()
