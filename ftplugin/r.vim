setlocal ts=2 sts=2 sw=2 expandtab
setlocal foldmethod=syntax
" automatic comment insertion: nice for roxygen2 docstrings
setlocal formatoptions+=cro
autocmd Filetype R,r,rmd,Rmd inoremap ,m <space>%>%<cr>
autocmd Filetype R,r,rmd,Rmd inoremap ,k <space>%>%
autocmd Filetype R,r,rmd,Rmd inoremap <M--> <space><-<space>
