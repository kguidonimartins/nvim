let mapleader=","

syntax on

filetype indent on
filetype on
" filetype plugin on

call plug#begin(system('echo -n "$PLUG_HOME"'))
" Plug 'neoclide/coc.nvim', { 'branch': 'release' }
Plug 'jpalardy/vim-slime'
Plug 'pacha/vem-tabline'
Plug 'ervandew/supertab'
Plug 'tpope/vim-commentary'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
call plug#end()

