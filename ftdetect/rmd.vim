autocmd BufNewFile,BufRead *.Rmd setlocal ft=rmd
autocmd BufNewFile,BufRead *.rmd setlocal ft=rmd

autocmd Filetype markdown,rmd inoremap ,r ```r<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,b ```bash<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,p ```python<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,c ```<cr>```<cr><cr><esc>2kO
autocmd Filetype markdown,rmd inoremap ,rr ```{r}<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,rb ```{bash}<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,rp ```{python}<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,rc ```<cr>```<cr><cr><esc>2kO

" autocmd Filetype rmd setlocal commentstring=<!--\ %s\ -->
