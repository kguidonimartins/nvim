autocmd BufNewFile,BufRead *.Rprofile set ft=r

autocmd BufRead DESCRIPTION set ft=r
autocmd BufRead Rprofile set ft=r
autocmd BufRead *.Rhistory set ft=r
autocmd BufNewFile,BufRead *.r set ft=r
autocmd BufNewFile,BufRead *.R set ft=r

autocmd BufNewFile,BufRead *.Rout set ft=rout
autocmd BufNewFile,BufRead *.Rout.save set ft=rout
autocmd BufNewFile,BufRead *.Rout.fail set ft=rout

autocmd BufNewFile,BufRead *.Rrst set ft=rrst
autocmd BufNewFile,BufRead *.rrst set ft=rrst

autocmd BufNewFile,BufRead *.Rmd set ft=rmd
autocmd BufNewFile,BufRead *.rmd set ft=rmd

autocmd Filetype markdown,rmd inoremap ,r ```r<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,b ```bash<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,p ```python<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,c ```<cr>```<cr><cr><esc>2kO
autocmd Filetype markdown,rmd inoremap ,rr ```{r}<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,rb ```{bash}<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,rp ```{python}<CR>```<CR><CR><esc>2kO
autocmd Filetype markdown,rmd inoremap ,rc ```<cr>```<cr><cr><esc>2kO

autocmd Filetype r setlocal commentstring=##\ %s
