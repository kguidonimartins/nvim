" General settings {{{

" CHECK: maybe I can use this: https://github.com/neovim/neovim/pull/13479#event-4813249467
let mapleader = ","
let maplocalleader = " "

syntax on

filetype on
filetype plugin on
filetype indent on

" }}}

" Plugin list {{{

call plug#begin(system('echo -n "$PLUG_HOME"'))

" Sorting plugins by name: select text plugin list, then `:sort i /^[^/]*/`

" Plug 'dense-analysis/ale', { 'do': 'pip install flake8 isort yapf' }
" Plug 'jiangmiao/auto-pairs'
" Plug 'vim-scripts/AutoComplPop'
" Plug 'ActivityWatch/aw-watcher-vim'
Plug 'APZelos/blamer.nvim'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-nvim-lsp-document-symbol'
Plug 'hrsh7th/cmp-nvim-lsp-signature-help'
Plug 'quangnguyen30192/cmp-nvim-tags'
Plug 'hrsh7th/cmp-path'
Plug 'lukas-reineke/cmp-rg'
Plug 'andersevenrud/cmp-tmux'
Plug 'hrsh7th/cmp-vsnip'
" Plug 'tjdevries/colorbuddy.nvim'
" Plug 'rhysd/committia.vim'
" Plug 'andersevenrud/compe-tmux', { 'branch': 'compe' }
" Plug 'github/copilot.vim'
" Plug 'ctrlpvim/ctrlp.vim'
Plug 'Mofiqul/dracula.nvim'
Plug 'gpanders/editorconfig.nvim'
Plug 'rafamadriz/friendly-snippets'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'tweekmonster/fzf-filemru'
Plug 'junegunn/fzf.vim'
Plug 'lewis6991/gitsigns.nvim'
" Plug 'junegunn/goyo.vim'
Plug 'lukas-reineke/indent-blankline.nvim'
" Plug 'junegunn/limelight.vim'
Plug 'nvim-lua/lsp-status.nvim'
Plug 'folke/lsp-trouble.nvim'
Plug 'ray-x/lsp_signature.nvim'
Plug 'onsails/lspkind-nvim'
" Plug 'glepnir/lspsaga.nvim'
Plug 'nvim-lualine/lualine.nvim'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug', 'rmd'] }
" Plug 'ishan9299/modus-theme-vim'
" Plug 'ishan9299/modus-theme-vim', {'branch': 'stable'}
Plug 'windwp/nvim-autopairs'
Plug 'akinsho/nvim-bufferline.lua'
Plug 'hrsh7th/nvim-cmp'
Plug 'norcalli/nvim-colorizer.lua'
" Plug 'hrsh7th/nvim-compe'
Plug 'bfredl/nvim-ipy', { 'do': ':UpdateRemotePlugins', 'for': 'python' }
Plug 'williamboman/nvim-lsp-installer'
Plug 'neovim/nvim-lspconfig'
" Plug 'jalvesaq/Nvim-R'
" Plug 'kyazdani42/nvim-tree.lua'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'nvim-treesitter/nvim-treesitter-textobjects'
Plug 'p00f/nvim-ts-rainbow' " performance issues
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kristijanhusak/orgmode.nvim'
Plug 'nvim-treesitter/playground'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-lua/popup.nvim'
" Plug 'jalvesaq/R-Vim-runtime'
" Plug 'mechatroner/rainbow_csv', { 'for': ['csv'] }
Plug 'tmhedberg/SimpylFold'
" Plug 'sanhajio/synonyms.vim'
" Plug 'godlygeek/tabular'
" Plug 'majutsushi/tagbar'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope-media-files.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'folke/todo-comments.nvim'
Plug 'folke/tokyonight.nvim', { 'branch': 'main' }
" Plug 'sirver/UltiSnips'
" Plug '907th/vim-auto-save'
Plug 'cormacrelf/vim-colors-github'
Plug 'tpope/vim-commentary'
" Plug 'will133/vim-dirdiff'
Plug 'junegunn/vim-easy-align'
Plug 'tpope/vim-fugitive'
" Plug 'airblade/vim-gitgutter'
" Plug 'takac/vim-hardtime'
Plug 'farmergreg/vim-lastplace'
" Plug 'tpope/vim-markdown'
" Plug 'plasticboy/vim-markdown'
" Plug 'vim-pandoc/vim-pandoc'
" Plug 'vim-pandoc/vim-pandoc-syntax'
" Plug 'sheerun/vim-polyglot'
" Plug 'tpope/vim-repeat'
Plug 'airblade/vim-rooter'
" Plug 'kshenoy/vim-signature'
Plug 'jpalardy/vim-slime'
" Plug 'honza/vim-snippets'
Plug 'tpope/vim-surround'
Plug 'tmux-plugins/vim-tmux-focus-events'
" Plug 'jmckiern/vim-venter'
" Plug 'mg979/vim-visual-multi', {'branch': 'master'}
" Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip'
" Plug 'hrsh7th/vim-vsnip-integ'
Plug 'wakatime/vim-wakatime'
Plug 'jreybert/vimagit'
" Plug 'vimwiki/vimwiki', { 'branch': 'dev' }
" Plug 'folke/which-key.nvim'
Plug 'folke/zen-mode.nvim'
Plug 'ziontee113/color-picker.nvim'
Plug 'stevearc/dressing.nvim'
Plug 'ziontee113/icon-picker.nvim'
Plug 'shivamashtikar/tmuxjump.vim'
call plug#end()

" }}}

" CHECK: https://github.com/pwntester/octo.nvim
" CHECK: https://github.com/tzachar/compe-tabnine
" CHECK: https://github.com/lspcontainers/lspcontainers.nvim
" CHECK: https://github.com/sindrets/diffview.nvim

" Lua config {{{

if has('nvim-0.5')
  lua << EOF
  require('plugin/nvim-lspconfig')
  require('plugin/gitsigns')
  -- require('plugin/nvim-compe')
  require('plugin/telescope')
  require('plugin/nvim-treesitter')
  require('nvim-autopairs')
  -- require('plugin/lspkind-nvim')
  -- require('plugin/lsp_signature')
  require('plugin/nvim-bufferline')
  require('plugin/lsp-status')
  require('plugin/nvim-web-devicons')
  require('plugin/nvim-tree')
  -- require('plugin/lsp-trouble')
  -- require('plugin/lspsaga')
  -- require('plugin/nvim-colorizer')
  -- require('plugin/nvim-ts-rainbow') -- performance issue
  -- require('plugin/todo-comments')
  require('plugin/zen-mode')
  require('plugin/orgmode')
  -- require('nvim-treesitter-playground')
  -- require('plugin/which-key')
  require('plugin/nvim-cmp')
  require('plugin/colorscheme')
  require('plugin/auto-change-colorscheme')
  require('plugin/lualine')
  -- require('plugin/statusline')
  require('plugin/pickers')
EOF
endif
" }}}

